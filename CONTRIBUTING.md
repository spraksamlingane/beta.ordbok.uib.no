Contributing
============

Changes are published using GitLab CI/CD. When making a change:

1. Test any changes to the frontend locally by runniing `npm run install` (if necessary) and `npm run serve`
2. Push changes to GitLab. When pipeline is finished, your changes will be visible on https://dev.ordbok.uib-no (check browser cache)
3. To publish your changes to beta, add a tag to your commit that starts with `release`, e.g. `release_2021-01-01`. When the pipeline finishes, changes up to that commit will be visible on https://beta.ordbok.uib.no

