import json
import urllib.parse

# Load lemmas
with open("bm_lemma.json") as inn:
    bm_list = json.load(inn)

with open("nn_lemma.json") as inn:
    nn_list = json.load(inn)



# Merge bm and nn
words = {}
for row in bm_list:
    words[row[0]] = words.get(row[0], set())
    words[row[0]].add(("bm", row[1]))

for row in nn_list:
    words[row[0]] = words.get(row[0], set())
    words[row[0]].add(("nn", row[1]))



url_counter = 0    
write_start = lambda out: out.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:xhtml=\"http://www.w3.org/1999/xhtml\">\n")
filenum = 0
out = open(f"src/sitemap/xml/sitemap_{filenum}.xml", "w")
write_start(out)

seen_articles = set()
for word, articles in words.items():
    if url_counter > 20000:
        out.write("</urlset>\n")
        url_counter = 0
        out.close()
        filenum += 1
        out = open(f"src/sitemap/xml/sitemap_{filenum}.xml", "w")
        write_start(out)
    
    
    for article in articles:
        if article[1] not in seen_articles:
            out.write("\t<url>\n")
            out.write(f"\t\t<loc>https://ordbokene.no/{article[0]}/{article[1]}</loc>\n")
            out.write(f"\t\t<xhtml:link rel=\"alternative\" href=\"https://ordbokene.no/{article[0]}/{article[1]}/{urllib.parse.quote(word)}\"/>\n")
            out.write("\t</url>\n")
            url_counter += 1
            seen_articles.add(article[1])

    # Omit articles
    if len(articles) > 1:
        out.write("\t<url>\n")
        out.write(f"\t\t<loc>https://ordbokene.no//bm,nn/{urllib.parse.quote(word)}</loc>\n")
        out.write("\t</url>\n")
        url_counter += 1

out.write("</urlset>\n")
out.close()


with open("src/sitemap/xml/sitemap_index.xml", "w") as outfile:
    outfile.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<sitemapindex xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n")
    outfile.write("<sitemap><loc>https://ordbokene.no/sitemap.xml</loc></sitemap>\n")

    for num in range(filenum+1):
        outfile.write(f"<sitemap><loc>https://ordbokene.no/sitemap_{num}.xml</loc></sitemap>\n")
    outfile.write("</sitemapindex>")